<div class="row col-lg-12 col-xs-12 col-md-12 contact">
	<div class="container center">
		<div class="contact-help">
			<div class="contact-help--block center">
				<div class="contact-help--title open-sans-regular">
					<h1>PODEMOS AJUDAR?</h1>
				</div>
			</div>
			<div class="contact-help--text open-sans-light  col-lg-6 center text-center">
				<p>Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non, arcu eu volutpat leo in vestibulum ullamcorper.</p>
			</div>
			<div class="contact-help--form col-lg-6 center">
				<form id="contato" method="post">
					<div>
						<input type="text" name="contato[nome]" placeholder="Nome" required>
					</div>
					<div>
						<input type="email" name="contato[email]" placeholder="E-mail" required>
					</div>
					<div>
						<textarea placeholder="Mensagem" name="contato[mensagem]" required></textarea>
					</div>
					<div class="right">
						<button type="submit">ENVIAR</button>
					</div>
					<div class="mensagem">
						<div id="EnvioOk" class="EnvioOk">
							<p>Mensagem enviada com sucesso !</p>
						</div>
						<div id="EnvioError" class="EnvioError">
							<p>Ocorreu um erro.</p>
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	while ( have_rows('email_header') ) : the_row();
        $email_contato = get_sub_field('texto_email', $postId);
            // display a sub field value 
    endwhile;
?> 
<?php
	//enviar
	// emails para quem será enviado o formulário

	if (isset($_REQUEST['contato']))
	{
	  $nome = $_REQUEST['contato']['nome'];
	  $email = $_REQUEST['contato']['email'];
	  $mensagem = $_REQUEST['contato']['mensagem'];


	  $emailenviar = $email_contato;
	  $destino = $emailenviar;
	  $assunto = "Contato pelo Site";

	  // É necessário indicar que o formato do e-mail é html
	  $headers  = 'MIME-Version: 1.0' . "\r\n";
	  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	  $headers .= 'From: '.$nome.' <'.$destino.'>'. "\r\n";
	  $headers .= 'Reply-To: <'.$email.'>'. "\r\n";
	  //$headers .= "Bcc: $EmailPadrao\r\n";
	  
	  

	  $msg = "";
	  $msg .= "Nome: ".$nome."<br />";
	  $msg .= "E-mail: ".$email."<br />";
	  $msg .= "Mensagem: ".$mensagem."<br />";


	  $enviaremail = mail($destino, $assunto, $msg, $headers);
	 
	  if($enviaremail){
	  	?>
	  	  <script type="text/javascript">
		  	  $(document).ready(function(){
			  	  	document.getElementById("EnvioOk").style.display = "block";
		  	  		/*simpleAlert({text: 'E-mail enviado com sucesso.', theme: 'success', temporary: true});*/
		  	  })  	    
	  	  </script>  	  
		<?php 
	 } 
	else {
	?>
		<script type="text/javascript">
	  	  $(document).ready(function(){
	  	  		document.getElementById("EnvioError").style.display = "block";
	  	  		/*simpleAlert({text: 'Deu ruim.', theme: 'error', temporary: true});*/
	  	  })  	    
	  	  </script> 
	<?php
	  	$mgm = "ERRO AO ENVIAR E-MAIL!";
	  }
	}
?>