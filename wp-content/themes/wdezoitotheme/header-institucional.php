<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png">
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.6.2.min.js"></script>
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
<body>
    <header class="row col-lg-12 col-xs-12 header institucional <?php if(!is_home() && !is_page('home')){ echo "header-interna"; } ?> ">
        <div class="row header-list">
            <div class="row header-list--container <?php if(is_page('institucional-ribeirao')){ echo "ribeirao"; } ?>" id="list">
                <div class="container center">
                    <div class="col-lg-2 left logo">
                        <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory')?>/images/Logo-JR.png"></a>                                   
                    </div>
                    <div class="col-lg-7 right menu">
                        <nav class="text-right open-sans-regular">
                            <?php 
                                $args = array("nome" => "Menu Principal", "tipo" => "list-block");
                                getMenu($args);
                            ?>
                        </nav>            
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <div class="row main <?php if(is_home() or is_page('home')){ echo "main-home"; }else{echo "main-internas";} ?>">