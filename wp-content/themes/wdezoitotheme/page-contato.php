<?php get_header(); ?>
	<div class="row contato">
		<div class="contato-container" id="page-contato">
			<div class="container center">
				<div class="contato-container--left col-lg-6 left">
					<div class="col-lg-10 center">
						<div class="contato-header">
							<h2>Podemos Ajudar?</h2>
						</div>
						<div class="contato-form">
							<form method="post">
								<fieldset>
									<legend class="text-center">
										<p>Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non.</p>										
									</legend>
									<div class="text-center">
										<p class="contato-form-feedback text-success" id="contato-feedback--success">
											<i class="fa fa-check-circle"></i>
											Mensagem entregue com sucesso.<br>
											<button type="button" class="btn btn-theme--red contato-show-form">Enviar nova mensagem</button>
										</p>
										<p class="contato-form-feedback text-danger" id="contato-feedback--danger">
											<i class="fa fa-exclamation-circle"></i>
											Aconteceu algum erro<br>
											<button type="button" class="btn btn-theme--red contato-show-form">Tentar novamente</button>
										</p>										
									</div>
									<div id="contato-inputs">
										<input type="hidden" name="contato[email][to]" value="" id="contato-form--emailSend" required>
										<div class="form-field">
											<input type="text" name="contato[nome]" id="txtNome" placeholder="Nome" required>
										</div>
										<div class="form-field">
											<input type="email" name="contato[email][by]" placeholder="E-mail" required>
										</div>
										<div class="form-field">
											<textarea  placeholder="Mensagem" name="contato[msg]" required></textarea>
										</div>
										<div class="form-field right">
											<button class="btn btn-theme--red">Enviar</button>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="contato-container--right col-lg-6 left">
					<div class="col-lg-10 center">
						<?php 
							$args 	= array("post_type" => "hoteis", "order" => "ASC");
							$query 	= new WP_Query($args);
							$hoteis = null;
							$i 		= 0;
							if($query->have_posts()){
								while($query->have_posts()){
									$query->the_post();
									$_hotel = get_post();
									$hoteis[$i]["id"] 						= $_hotel->ID;
									$hoteis[$i]["contato"]["telefone"] 		= get_field("hotel-telefone", $_hotel->ID);
									$hoteis[$i]["contato"]["email"] 		= get_field("hotel-email", $_hotel->ID);
									$hoteis[$i]["localizacao"]["cep"] 		= get_field("hotel-cep", $_hotel->ID);
									$hoteis[$i]["localizacao"]["cidade"]	= $_hotel->post_title;
									$hoteis[$i]["localizacao"]["endereco"] 	= get_field("hotel-endereco", $_hotel->ID);
									$hoteis[$i]["localizacao"]["bairro"]	= get_field("hotel-bairro", $_hotel->ID);
									$hoteis[$i]["localizacao"]["latitude"]	= get_field("hotel-latitude", $_hotel->ID);
									$hoteis[$i]["localizacao"]["longitude"]	= get_field("hotel-longitude", $_hotel->ID);
									$hoteis[$i]["social"]["facebook"]		= get_field("hotel-link--facebook", $_hotel->ID);
									$hoteis[$i]["social"]["instagram"]		= get_field("hotel-link--instagram", $_hotel->ID);
									$hoteis[$i]["social"]["twitter"]		= get_field("hotel-link--twitter", $_hotel->ID);
									$hoteis[$i]["social"]["pinterest"]		= get_field("hotel-link--pinterest", $_hotel->ID);
									$i++;
								}
							}
							wp_reset_query();
						?>
						<div class="row hoteis">
							<?php if($hoteis != null): ?>
								<div class="hoteis-list">
									<?php 
										$i= 0; 
										$initForm = null;
										foreach($hoteis as $hotel): 
											if($i == 0){
												$initForm["latitude"] = $hotel['localizacao']['latitude'];
												$initForm["longitude"] = $hotel['localizacao']['longitude'];
												$initForm["email"]["to"] = $hotel['contato']['email'];
											}
									?>
										<a href="#" data-cod="<?php echo $hotel['id']; ?>" id="hotel--<?php echo $hotel['id']; ?>" class="hoteis-hotel <?php if($i == 0){ echo 'active'; } ?>">
											<input type="hidden" value="<?php echo $hotel['localizacao']['latitude']; ?>" id="hotel-latitude--<?php echo $hotel['id']; ?>">
											<input type="hidden" value="<?php echo $hotel['localizacao']['longitude']; ?>" id="hotel-longitude--<?php echo $hotel['id']; ?>">
											<input type="hidden" value="<?php echo $hotel['contato']['email']; ?>" id="hotel-email--<?php echo $hotel['id']; ?>">
											<h3><?php echo $hotel['localizacao']['cidade']; ?></h3>
											<p>
												<i class="fa fa-map-marker"></i>
												<?php echo $hotel['localizacao']['endereco']."|".$hotel['localizacao']['bairro'] ; ?>
											</p>
											<ul class="list list-items--inline hoteis-hotel-contato">
												<li class="list--item">
													<span class="hoteis-hotel-contato--link" title="Ligue para: <?php echo $hotel['contato']['telefone']; ?>"><i class="fa fa-phone"></i><?php echo $hotel['contato']['telefone']; ?></span>
												</li>
												<li class="list--item">
													<span class="hoteis-hotel-contato--link" title="Nos mande um e-mail para: <?php echo $hotel['contato']['email']; ?>"><i class="fa fa-envelope"></i><?php echo $hotel['contato']['email']; ?></span>
												</li>
											</ul>
										</a>
									<?php 
										$i++; 
										endforeach; 
									?>
								</div>
							<?php endif; ?>
						</div>
						<div class="social-block text-center">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-instagram"></i></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-pinterest-p"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row map">
		<div id="maps"></div>
	</div>
<?php get_footer(); ?>
<?php if($initForm != null): ?>
	<script type="text/javascript">
		$(window).load(function(){
			document.getElementById("contato-form--emailSend").value = "<?php echo $initForm['email']['to']; ?>";
			newMaps("<?php echo $initForm['latitude']; ?>", "<?php echo $initForm['longitude']; ?>");
		});
	</script>
<?php endif; ?>
<?php 
	if(isset($_POST['contato'])){
		require 'phpmailer/class.phpmailer.php';
		require 'phpmailer/class.smtp.php';
		
		$mail = new PHPMailer();
		//$mail->isMail();
		
		$mail->IsSMTP();
        $mail->SMTPAuth   = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host       = "smtp.gmail.com";
		$mail->Port       = 465;  
		$mail->Username   = "suingacervejaria@gmail.com";
		$mail->Password   = "v5&W8T#3L9q";

		$form_post 			= $_POST['contato'];
		$nome 				= $form_post['nome'];
		$email  			= $form_post['email']['by'];
		$mensagem  			= $form_post['msg'];

	  	//$emailenviar = "guilherme@cervejariasuinga.com.br";
	  	$emailenviar = "enriqueneto65@gmail.com";
	  	$destino = $emailenviar;
	  	$assunto = "Contato feito pelo site";
	  	// É necessário indicar que o formato do e-mail é html
	  	$headers  = 'MIME-Version: 1.0' . "\r\n";
	  	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	  	$headers .= 'From: '.$nome.' <'.$destino.'>'. "\r\n";
	  	$headers .= 'Reply-To: <'.$email.'>'. "\r\n";
	  	//$headers .= "Bcc: $EmailPadrao\r\n";

		$msg = "";
		$msg .= "<strong>Dados de Contato</strong><br>";
		$msg .= "<strong>Nome:</strong> ".$nome."<br />";
		$msg .= "<strong>E-mail:</strong> ".$email."<br />";
		$msg .= "<strong>Mensagem:</strong> ".$mensagem."<br />";


		$mail->setFrom('suingacervejaria@gmail.com', $nome);
		
		//$mail->addAddress('contato@cervejariasuinga.com.br', 'Cerverjaria Suinga');
		$mail->addAddress($email_to, 'JR Hoteis');

		//$mail->addAddress('marcelolauton@wdezoito.com.br', 'Marcelo Lauton');
		$mail->isHTML(true);

		$mail->Subject = $assunto;
		$mail->Body    = $msg;
		$mail->AltBody = $msg;

  		if($mail->send()){
  			echo "<script>document.getElementById('contato-feedback--success').style.display = 'block'; document.getElementById('contato-inputs').style.display = 'none';</script>";
  		}else{
  			echo "<script>document.getElementById('contato-feedback--danger').style.display = 'block'; document.getElementById('contato-inputs').style.display = 'none';</script>";
  		}
	} 
?>