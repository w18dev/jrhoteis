<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
<body>
    <?php 
        $infoRede   = getInfoRede();
        $headerLogo = $infoRede["logo"];
    ?>
    <header class="header <?php if(!is_home() && !is_page('home')){ echo "header-interna"; } ?>" id="navigation" role="navigation">
        <div class="header-toolbar">
            <div class="container center">
                <div class="left">
                    <form class="search" method="post" action="">
                        <div class="input-group input-group-btn--left">
                            <div class="form-field">
                                <input type="text" name="s" placeholder="Pesquisa" required class="field-input">
                            </div>
                            <button type="submit" class="input-group-btn"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="right text-right">
                    <ul class="header-toolbar-nav list list-items--inline">
                        <li class="list--item">
                            <a href="<?php bloginfo('url'); ?>/contato">
                                <i class="icon icon-marker-white"></i>
                                Onde nos encontrar?
                            </a>
                        </li>
                        <?php if(!empty($infoRede['email'])): ?>
                            <li class="list--item">
                                <a href="mailto:<?php echo $infoRede['email']; ?>">
                                    <i class="icon icon-envelope-white"></i>
                                    <?php echo $infoRede['email']; ?>    
                                </a>
                            </li> 
                        <?php endif; ?>
                    </ul> 
                </div> 
            </div>
        </div>
        <div class="header-nav">
            <div class="container center">
                <div class="left">
                    <h1 class="logo">
                        <a class="logo-link" href="<?php bloginfo('url'); ?>">
                            <img class="logo-brand" src="<?php echo $headerLogo; ?>">
                            <span class="logo-name"><?php bloginfo('name'); ?></span>
                        </a>
                    </h1>
                </div>
                <div class="right text-right">
                    <?php 
                        $args = array("nome" => "Menu Principal", "tipo" => "list-inline");
                        getMenu($args);
                    ?>
                </div>
            </div>
        </div>
    </header>
    
    <div class="row main <?php if(is_home() or is_page('home')){ echo "main-home"; } ?>">
