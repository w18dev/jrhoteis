<?php include('header.php'); ?>

	<div class="row col-lg-12 col-xs-12 col-md-12 quartos">
		<div class="container center">
			<div class="quartos-block--left col-lg-3 container left">
				<div class="filter">
					<div class="filter-block text-center">
						<div class="row filter-form--basic">
							<div class="filter-title open-sans-regular">
								<h1>Filtro básico</h1>
							</div>
							<form>
								<div class="form-field">
									<div class="form-field--icon-hotel">
										<img src="<?php bloginfo('template_directory')?>/images/icons/hotel.png">
									</div>
									<input type="text" name="" placeholder="Qual hotel?">
								</div>
								<div class="form-field">
									<div class="form-field--icon-quarto">
										<img src="<?php bloginfo('template_directory')?>/images/icons/quarto.png">
									</div>
									<input type="text" name="" placeholder="Tipo de quarto">
								</div>
							</form>
						</div>
						<div class="row filter-form--advanced">
							<div class="filter-title open-sans-regular">
								<h1>Filtro Avançado</h1>
							</div>
							<form>
								<div class="form-field">
									<div class="form-field--icon-agenda">
										<img src="<?php bloginfo('template_directory')?>/images/icons/calendario.png">
									</div>
									<input type="text" name="" placeholder="Check-in">
								</div>
								<div class="form-field">
									<div class="form-field--icon-agenda">
										<img src="<?php bloginfo('template_directory')?>/images/icons/calendario.png">
									</div>
									<input type="text" name="" placeholder="Check-out">
								</div>
								<div class="form-field">
									<div class="form-field--icon-hospede">
										<img src="<?php bloginfo('template_directory')?>/images/icons/user.png">
									</div>
									<input type="text" name="" placeholder="Hóspedes">
								</div>
							</form>
							<div class="row filter-services">
								<div class="row filter-services--title open-sans-regular">
									<div class="left">
										<img src="<?php bloginfo('template_directory')?>/images/star.png">
									</div>
									<div class="left">
										<p>Serviços</p>
									</div>
								</div>
								<div class="row filter-services--block col-lg-12">
									<div class="block-service left col-lg-3">
										<div class="block-service--icon">
											<label>
												<input type="radio" name="servicoQuarto" value="" />
												<i class="fa fa-coffee" ></i>
												<div class="block-service--desc">
													<p>Serviço de quarto</p>
												</div>
											</label>
										</div>
									</div>
									<div class="block-service left col-lg-3 ">
										<div class="block-service--icon">
											<label>
												<input type="radio" name="wifi" value="" />
												<i class="fa fa-wifi" ></i>
												<div class="block-service--desc">
													<p>Wifi</p>
												</div>
											</label>
										</div>
									</div>
									<div class="block-service left col-lg-3">
										<div class="block-service--icon">
											<label>
												<input type="radio" name="garagem" value="" />
												<i class="fa fa-car"></i>
												<div class="block-service--desc">
													<p>Garagem</p>
												</div>
											</label>
										</div>
									</div>
									<div class="block-service left col-lg-3">
										<div class="block-service--icon">
											<label>
												<input type="radio" name="tv" value="" />
												<i class="fa fa-television"></i>
												<div class="block-service--desc">
													<p>TV a cabo</p>
												</div>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="block-service--button open-sans-regular">
							<button type="submit">Buscar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="quartos-block--right col-lg-9 container left">
				<div class="filter">
					<div class="row filter-block col-lg-7">
						<div class="container">
							<div class="filter-title left col-lg-4">
								<h1>Ordenar por: </h1>
							</div>
							<div class="filter-selects left col-lg-8">
								<div class="select left col-lg-6">
									<select>
										<option>Cidade</option>
									</select>
								</div>
								<div class="select left col-lg-5">
									<select>
										<option>Avaliação</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row quarto col-lg-12">
					<div class="quarto-container">
						<?php 
							   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
							   $query = array("post_type" => "categoria", "post_per_page" => 3,"post_status" => "publish", "paged" => $paged);
	                           query_posts($query);
	                           while(have_posts()): the_post(); 
	                           	  $post_ID = $post->ID;
	                          	  $post = get_post();
	                          	  $imagem = get_field('imagem_quarto', $post_ID);
	                          	  $local = get_field('local_quarto', $post_ID);
	                              $conteudo_resumo = getResume($post->post_content);  
	                              $post_comment_count		= wp_count_comments($post->ID)->all;   
	                     ?>
							 <div class="quarto-post col-lg-4 left">
							 	<div class="quarto-post--img">
							 		<img src="<?php echo $imagem; ?>">
							 	</div>
							 	<div class="container open-sans-regular">
								 	<div class="quarto-post--title">
								 		<h1><?php the_title(); ?></h1>
								 	</div>
								 	<div class="row quarto-post--info1">
									 	<div class="quarto-post--hotel left">
									 		<h2><?php echo $local; ?></h2>
									 	</div>
								 	</div>
								 	<div class="row quarto-post--info2">
									 	<div class="quarto-post--services left">
									 		<?php
											    while ( have_rows('servicos') ) : the_row();
												$postId = $post->ID;
												$imagem_icone = get_sub_field('icone_servico', $postId); 
										    ?>
										 		<div class="icon-service left">
										 			<img src="<?php echo $imagem_icone; ?>">
										 		</div>
									 		<?php endwhile; ?>					 		
									 	</div>		
								 	</div>
								 	<div class="row quarto-post--avaliacao">
								 		<div class="avaliacao left">
								 			<p>Avaliações</p>
								 		</div>
								 		<div class="stars right">
								 			<img src="<?php bloginfo('template_directory'); ?>/images/stars-quarto.png">
								 		</div>
								 	</div>
								 	<div class="row quarto-post--desc">
								 		<p><?php echo $conteudo_resumo; ?></p>
								 	</div>
								 	<div class="row quarto-post--link text-center">
								 		<a href="<?php the_permalink(); ?>">Reserve aqui</a>
								 	</div>
							 	</div>
							 </div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

























<?php include('footer.php'); ?>