<div class="row comments col-lg-12 col-xs-12 col-md-12">
	<div class="comments-bkg bkg">
		<?php 
			$comentarios = Comentarios::getComentariosHome();
		?>
		<div class="container center">
			<?php if($comentarios != null): ?>
				<div class="col-lg-4 container left space">
					<div class="comments-users">
						<div class="row comments-users--align">
							<div class="col-lg-3 left container">
								<div class="comments-users--img">
									<img src="<?php bloginfo('template_directory')?>/images/user.jpg">
								</div>
							</div>
							<div class="col-lg-9 right ">
								<div class="comments-users--name open-sans-regular">
									<h1>LOREM IPSUM DOLOR</h1>
								</div>
								<div class="col-lg-12 open-sans-light">
									<div class="comments-users--age left col-lg-3">
										<p>22 anos,</p>
									</div>
									<div class="comments-users--city left col-lg-9">
										<p> Pres. Prudente - SP</p>
									</div>
								</div>
								<div class="comments-users--stars text-right container">
									<img src="<?php bloginfo('template_directory')?>/images/stars.png">
								</div>
							</div>
						</div>
						<div class="row comments-users--content col-lg-12 container open-sans-light">
							<p>“Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non, arcu eu volutpat leo in vestibulum ullamcorper”</p>
						</div>
					</div>
				</div>
				<!-- REPLACE -->
				<div class="col-lg-4 container left space">
					<div class="comments-users">
						<div class="row comments-users--align">
							<div class="col-lg-3 left container">
								<div class="comments-users--img">
									<img src="<?php bloginfo('template_directory')?>/images/user.jpg">
								</div>
							</div>
							<div class="col-lg-9 right ">
								<div class="comments-users--name open-sans-regular">
									<h1>LOREM IPSUM DOLOR</h1>
								</div>
								<div class="col-lg-12 open-sans-light">
									<div class="comments-users--age left col-lg-3">
										<p>22 anos,</p>
									</div>
									<div class="comments-users--city left col-lg-9">
										<p> Pres. Prudente - SP</p>
									</div>
								</div>
								<div class="comments-users--stars text-right container">
									<img src="<?php bloginfo('template_directory')?>/images/stars.png">
								</div>
							</div>
						</div>
						<div class="row comments-users--content col-lg-12 container open-sans-light">
							<p>“Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non, arcu eu volutpat leo in vestibulum ullamcorper”</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 container left space">
					<div class="comments-users">
						<div class="row comments-users--align">
							<div class="col-lg-3 left container">
								<div class="comments-users--img">
									<img src="<?php bloginfo('template_directory')?>/images/user.jpg">
								</div>
							</div>
							<div class="col-lg-9 right ">
								<div class="comments-users--name open-sans-regular">
									<h1>LOREM IPSUM DOLOR</h1>
								</div>
								<div class="col-lg-12 open-sans-light">
									<div class="comments-users--age left col-lg-3">
										<p>22 anos,</p>
									</div>
									<div class="comments-users--city left col-lg-9">
										<p> Pres. Prudente - SP</p>
									</div>
								</div>
								<div class="comments-users--stars text-right container">
									<img src="<?php bloginfo('template_directory')?>/images/stars.png">
								</div>
							</div>
						</div>
						<div class="row comments-users--content col-lg-12 container open-sans-light">
							<p>“Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non, arcu eu volutpat leo in vestibulum ullamcorper”</p>
						</div>
					</div>
				</div>
				<!-- END REPLACE -->
			<?php else: ?>
				<div>
					<blockquote class="blockquote">
						<p>
							"As pessoas não sabem oque querem até você mostrar a elas."<br>
							<small>Steve Jobs, fundador da Apple</small>
						</p>
					</blockquote>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>