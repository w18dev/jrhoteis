<div id="comments">
	<?php
 		$req = get_option('require_name_email'); // Verifica se os campos são obrigatórios.
 		if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) 
 			die('Please do not load this page directly. Thanks!');

 		if ( ! empty($post->post_password) ) :
  			if ( $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password ) :
	?>
    			<div class="nopassword">
    				<?php _e('This post is password protected. Enter the password to view any comments.', 'seu-template') ?>
    			</div>
</div>
	<?php
  				return;
			endif;
		endif;
	?>
 	<div id="respond" class="comment-respond">
        <h3>Deixe um comentário</h3>
	    <div class="formcontainer">
	    	<form id="comment-form" class="comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
				<?php if ( $user_ID ) : ?>
		       		<p id="login">
		       			<?php 
		       				printf(__('<span class="loggedin">Logged in as <a href="%1$s" title="Logged in as %2$s">%2$s</a>.</span> <span class="logout"><a href="%3$s" title="Log out of this account">Log out?</a></span>', 'seu-template'),
		        			get_option('siteurl') . '/wp-admin/profile.php',
		        			wp_specialchars($user_identity, true),
		        			wp_logout_url(get_permalink()) ) 
		        		?>        		
		        	</p> 
				<?php else : ?>
			        <div id="form-section-author" class="row form-section--nome left col-lg-6">
			        	<div class="row form-input container">
			        		<input id="author" name="author" placeholder="Nome Completo" type="text" value="<?php echo $comment_author ?>" size="30" maxlength="20" tabindex="3" />
			        	</div>
			        </div> 
		        	<div id="form-section-email" class="row form-section--email left  col-lg-6">
			        	<div class="row form-input container">
			        		<input id="email" name="email" placeholder="E-mail" type="text" value="<?php echo $comment_author_email ?>" size="30" maxlength="50" tabindex="4" />
			        	</div>
		        	</div> 
		        <?php endif; ?>
	            <div id="form-section-comment" class="row form-section--textarea">
	        		<div class="row form-textarea col-lg-12">
	        			<textarea id="comment" name="comment" placeholder="Escreva seu comentário aqui" cols="45" rows="8" tabindex="6"></textarea>
	        		</div>
	            </div> 
				<?php do_action('comment_form', $post->ID); ?> 
	       		<div class="form-submit right">
		       		<button id="submit" name="submit" type="submit" tabindex="7" class="btn btn-theme--blue">
		       			ENVIAR
		       		</button>
	       			<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
	       		</div>
				<?php comment_id_fields(); ?>   
	      	</form>
	    </div>
	</div>
<?php 
	if (have_comments()): 
		$ping_count = $comment_count = 0;
		if(count($comments) > 0):
?>
	   		<div id="comments-list" class="comments-list">
     			<h3><?php echo count($comments); ?> Comentários</h3>
				<?php 
					$total_pages = get_comment_pages_count(); 
					if ( $total_pages > 1 ) : 
				?>
					<div id="comments-nav-above" class="comments-navigation">
					   <div class="paginated-comments-links">
					   	<?php paginate_comments_links(); ?>
					  	</div>
					</div>
				<?php endif; ?>    
				<ol>
					<?php
						foreach ( $comments as $comment ):
					?>
						<li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
							<div class="comment-header">
								<h4 class="comment-author"><?php comment_author(); ?></h4>
								<p class="comment-publish"><span class="comment-publish--date"><?php echo get_comment_date(); ?></span> às <span class="comment-publish--time"><?php echo get_comment_time(); ?></span></p>
							</div>
							<div class="comment-content">
								<?php comment_text(); ?>
							</div>
						</li>
					<?php
						endforeach;
					?>
				</ol> 
				<?php 
					$total_pages = get_comment_pages_count(); 
					if ( $total_pages > 1 ) : 
				?>
				    <div id="comments-nav-below" class="comments-navigation">
				      	<div class="paginated-comments-links">
				      		<?php paginate_comments_links(); ?>
				      	</div>
				    </div>
				<?php 
					endif; 
				?>  
    		</div> 
<?php 
		endif;
	endif;
?>

</div>