<?php include('header.php'); ?>
	<?php
		$args 		= array("post_type" => "page", "name" => "home");
		$query 		= new WP_Query($args);
		$homeInfo  	= null;
		if($query->have_posts()){
			$query->the_post();
			$_home 									= get_post();
			$homeInfo["id"] 						= $_home->ID;
			$homeInfo["cover"]["background_url"]	= get_field('home-cover--background', $_home->ID);
			$homeInfo["cover"]["titulo"]			= get_field('home-cover--titulo', $_home->ID);
			$homeInfo["cover"]["descricao"]			= get_field('home-cover--descricao', $_home->ID);
		}
		wp_reset_query();
	?>
	<div class="cover cover-home cover-background" style="background-image: url(<?php echo $homeInfo['cover']['background_url']; ?>)">
        <div class="container center">
            <div class="text-center">
                <h2><?php echo $homeInfo["cover"]["titulo"]; ?></h2>
                <p><?php echo $homeInfo["cover"]["descricao"]; ?></p>
            </div>
        </div> 
	</div>
	<div class="reserva reserva-home">
		<div class="container center">
			<div class="reserva-container">		        
		        <form class="reserva-form" method="post" action="<?php bloginfo('url'); ?>/">
		        	<fieldset>
		        		<legend class="container"><h2>O jeito mais rápido de fazer sua reserva</h2></legend>
		        		<div class="row">
			                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 container"> 
			                	<?php 
			                		$cidades =  Hoteis::getCidades();
			                	?>
			                	<div class="form-select">
			                		<input type="hidden" id="reserva-hotel--request" name="reserva[hotel]">			                		
				                    <div class="field-container">
				                    	<button class="form-action" id="reserva-select--hotel" type="button">Qual Hotel?</button>
				                    	<?php if($cidades != null): ?>
					                    	<ul class="field-list list list-items--block">
					                    		<?php foreach($cidades as $cidade): ?>
						                    		<li class="list--item">
						                    			<a href="#" class="field-list-item" data-value="<?php echo $cidade; ?>"><?php echo $cidade; ?></a>
						                    		</li>
					                    		<?php endforeach; ?>
					                    	</ul>
				                    	<?php endif; ?>
				                    </div>
				                    <i class="icon icon-hotel-gray"></i>
			                	</div>
			                </div>
			                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 container">
			                	<div class="form-field">
				                    <input type="text" name="txtCheckIn" placeholder="Check-in" id="datepicker" class="field-input datepicker">			                		
			                		<i class="icon icon-calendar-gray"></i>
			                	</div>
			                </div>
			                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 container">
			                	<div class="form-field">
				                    <input type="text" name="txtCheckOut" placeholder="Check-out" id="datepicker2" class="datepicker">			                		
			                		<i class="icon icon-calendar-gray"></i>
			                	</div>
			                </div>
			                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 container">
			                	<div class="form-select">
				                    <input type="hidden" name="reserva[hospede][adulto]" id="reserva-hospede-adulto--request"  value="0">
				                    <input type="hidden" name="reserva[hospede][crianca]" id="reserva-hospede-crianca--request" value="0">
				                    <input type="hidden" name="reserva[hospede][bebe]" id="reserva-hospede-bebe--request" value="0">
				                    <div class="field-container">
				                    	<button class="form-action" id="reserva-select--hospede" type="button">Hospede</button>
				                    	<ul class="field-list list list-items--block">
				                    		<li class="list--item">				                    			
						                    	<div class="field-list-item">
						                    		<div class="hospede-combo">
						                    			<div class="row">
						                    				<div class="col-xs-4">
						                    					<p class="hospede-combo-label"><span id="reserva-hospede-adulto--count">0</span> adulto</p>
						                    				</div>
						                    				<div class="col-xs-8 text-right">
						                    					<div class="hospede-combo-buttons">
						                    						<button type="button" class="hospede-combo-button add" data-target="adulto"><i class="fa fa-plus"></i></button>
						                    						<button type="button" class="hospede-combo-button remove" data-target="adulto"><i class="fa fa-minus"></i></button>
						                    					</div>
						                    				</div>
						                    			</div>
						                    		</div>
						                    	</div>
				                    		</li>
				                    		<li class="list--item">				                    			
						                    	<div class="field-list-item">
						                    		<div class="hospede-combo">
						                    			<div class="row">
						                    				<div class="col-xs-4">
						                    					<p class="hospede-combo-label"><span id="reserva-hospede-crianca--count">0</span> crianças</p>
						                    				</div>
						                    				<div class="col-xs-8 text-right">
						                    					<p class="hospede-combo-label--small">2 a 12 anos</p>
						                    					<div class="hospede-combo-buttons">
						                    						<button type="button" class="hospede-combo-button add" data-target="crianca"><i class="fa fa-plus"></i></button>
						                    						<button type="button" class="hospede-combo-button remove" data-target="crianca"><i class="fa fa-minus"></i></button>
						                    					</div>
						                    				</div>
						                    			</div>
						                    		</div>
						                    	</div>
				                    		</li>
				                    		<li class="list--item">				                    			
						                    	<div class="field-list-item">
						                    		<div class="hospede-combo">
						                    			<div class="row">
						                    				<div class="col-xs-4">
						                    					<p class="hospede-combo-label"><span id="reserva-hospede-bebe--count">0</span> bebês</p>
						                    				</div>
						                    				<div class="col-xs-8 text-right">
						                    					<p class="hospede-combo-label--small">menor de 2</p>
						                    					<div class="hospede-combo-buttons">
						                    						<button type="button" class="hospede-combo-button add" data-target="bebe"><i class="fa fa-plus"></i></button>
						                    						<button type="button" class="hospede-combo-button remove" data-target="bebe"><i class="fa fa-minus"></i></button>
						                    					</div>
						                    				</div>
						                    			</div>
						                    		</div>
						                    	</div>
				                    		</li>
					                    </ul>
				                    </div>	
				                    <i class="icon icon-hospede-gray"></i>		                		
			                	</div>
			                </div>
			                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 container">
			                    <button type="submit" name="buscar" class="btn btn-theme--red">Buscar</button>
			                </div>		        			
		        		</div>
		        	</fieldset>
		        </form>				
			</div>
	    </div>
    </div>
	<div class="section nossos-hoteis">
		<div class="container center"> 
			<div class="section-header">
				<div class="section-header-title text-center">
					<h2>Nossos Hotéis</h2>
				</div>
			</div>
			<div class="nossos-hoteis-list">
				<?php 
					$hoteis = Hoteis::getHoteis();
				?>
				<div class="row">
					<?php if($hoteis != null): ?>
						<?php foreach($hoteis as $hotel): ?>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 container">
								<div class="nossos-hoteis-item">
									<div class="nossos-hoteis-foto">
										<img src="<?php echo $hotel['foto']; ?>" alt="Foto do Hotel de <?php echo $hotel['localizacao']['cidade']; ?>">
									</div>
									<div class="nossos-hoteis-content">
										<h3 class="nossos-hoteis-cidade">
											<?php echo $hotel['localizacao']['cidade']; ?>											
										</h3>
										<ul class="nossos-hoteis-detalhes list list-items--block">
											<li class="list--item" title="<?php echo $hotel['localizacao']['endereco']; ?> - <?php echo $hotel['localizacao']['bairro']; ?>"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $hotel['localizacao']['endereco']; ?> - <?php echo $hotel['localizacao']['bairro']; ?></li>
											<li class="list--item"><i class="fa fa-phone"></i><?php echo $hotel['contato']['telefone']; ?></li>
											<li class="list--item"><i class="fa fa-envelope"></i><?php echo $hotel['contato']['email']; ?></li>
										</ul>
										<a href="<?php bloginfo('url'); ?>/?cidade=" class="btn btn-block btn-theme--red">Ver os quartos</a>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			
		</div>
	</div>
	<div class="row galeria col-lg-12 col-xs-12 col-md-12">
		<div class="container center">
			<div class="galeria-container">
				<div class="grid">
					<div class="grid-control">
					<?php
					  while ( have_rows('galeria') ) : the_row();
					        $post_Id = $post->ID;
					        $imagem_grid = get_sub_field('imagem_galera', $post_Id);
					?>
						<div class="item">
							<img src="<?php echo $imagem_grid; ?>">	
						</div>
					<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('comentarios.php'); ?>
	<div class="section servicos">
		<div class="container center">
			<div class="section-header">
				<div class="section-header-title">
					<h2>Nossos Serviços</h2>
				</div>
			</div>
			<?php 
				$servicos = Servicos::getServicos();
			?>
			<?php if($servicos != null): ?>
				<div class="row servicos-list">
					<?php foreach($servicos as $servico): ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center container">
						<div class="servicos-item">
							<div class="servicos-item-icon">
								<img src="<?php echo $servico['icone']; ?>" alt="Ícone referente a <?php echo $servico['nome']; ?>">
							</div>
							<h3 class="servicos-item-titulo"><?php echo $servico['nome']; ?></h3>	
							<p class="servicos-item-descricao"><?php echo $servico['descricao']; ?></p>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php include('footer.php'); ?>