<?php include('header.php'); ?>
	<div class="row col-lg-12 col-xs-12 blog">
		<?php include ('blog-banner.php'); ?>
		<div class="row col-lg-12 col-xs-12 blog-container">
			<div class="container center">
				<div class="row col-lg-9 left block-left">
					<div class="single col-lg-12">
						<?php 
							the_post();
							$post = get_post();        
							$postId = $post->ID;
							$titulo = get_the_title();	
							$conteudo = get_the_content();
						?>
						<div class="container center">
							<div class="row">
								<div class="single-title text-center mongolian">
									<h1><?php echo $titulo; ?></h1>
								</div>
								<div class="row col-lg-12 center single-block">
									<div class="col-lg-6 text-right single-block--author">
										<p>por <?php the_author(); ?> /</p>
									</div>
									<div class="col-lg-6 text-left single-block--category">
										<?php the_category(); ?>
									</div>
								</div>	
							</div>
						</div>
						<div class="carousel-single">
							<div class="row single-posts" id="carousel-single">
								<?php while ( have_rows('carousel_posts_blog') ) : the_row();
									$imagem_carousel = get_sub_field('imagem_carousel_posts', $postId); 
							    ?>
									<div class="item">								
										<div class="carousel-img">
											<img src="<?php echo $imagem_carousel; ?>">
										</div>
									</div>
							    <?php endwhile; ?>
							</div>
							<div class="col-lg-11 center">
								<div class="row single-content">
									<p><?php echo $conteudo; ?></p>
								</div>
								<div class="row single-comments">
									<?php comments_template(); ?> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include('blog-right.php'); ?>
			</div>
		</div>
	</div>





<?php include('footer.php'); ?>