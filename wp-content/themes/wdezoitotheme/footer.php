
</div>
<footer class="row footer col-lg-12 col-xs-12 col-md-12">
	<div class="footer-container">
		<div class="footer-block container center">
			<div class="footer-about col-lg-3 left">
				<div class="col-lg-10">
					<div class="footer-about--title open-sans-semi">
						<h1>Sobre nós</h1>
					</div>
					<div class="footer-about--content open-sans-regular">
						<p>Lorem ipsum dolor sit amet, faucibus eget massa mauris magna magna, ac aliquam soluta interdum, amet quo sed, cras praesent id. Ut maecenas erat sem lacinia non, arcu eu volutpat leo in vestibulum ullamcorper</p>
					</div>
				</div>
			</div>
			<div class="footer-blog col-lg-3 left">
				<div class="col-lg-10">
					<div class="footer-blog--title open-sans-semi">
						<h1>BLOG</h1>
					</div>
					<?php 
						   $query = array("post_type" => "post", "showposts" => 3,"post_status" => "publish");
                           query_posts($query);
                           while(have_posts()): the_post(); 
                          	  $post = get_post();
                              $footer_blog_resumo = getResumeMin($post->post_content);
                              $footer_blog_title = $post->post_title;   
                              while ( have_rows('carousel_posts_blog') ) : the_row();
		                  	  		$footer_blog_image = get_sub_field('imagem_carousel_posts', $postId); 
						      endwhile; 
                     ?>
						<div class="row footer-blog--posts">
                 			<a href="<?php the_permalink(); ?>">
								<div class="col-lg-4">
									<div class="post-img left">
										<img src="<?php echo $footer_blog_image; ?>">
									</div>
								</div>
								<div class="col-lg-8">
									<div class="post-content left">
										<div class="post-content--title open-sans-light">
											<h1><?php echo $footer_blog_title; ?></h1>
										</div>
										<div class="post-content--text open-sans-light">
											<p><?php echo $footer_blog_resumo; ?></p>
										</div>
									</div>	
								</div>
							</a>
						</div>
					<?php endwhile; ?>	
				</div>
			</div>
			<div class="footer-contact col-lg-6 left">
				<div class="footer-contact--title open-sans-semi">
					<h1>CONTATO</h1>
				</div>
				<div class="row footer-contact--align">
					<div class="footer-contact--container open-sans-light left col-lg-4">
						<div class="contact-city">
							<p>Pres. Prudente - SP</p>
						</div>	
						<div class="contact-phone">
							<i class="fa fa-phone"></i><p>(99) 9999-9999</p>
						</div>
						<div class="contact-mail">
							<i class="fa fa-envelope"></i><p>email@email.com.br</p>
						</div>				
					</div>
					<div class="footer-contact--container open-sans-light left col-lg-4">
						<div class="contact-city">
							<p>Marília - SP</p>
						</div>	
						<div class="contact-phone">
							<i class="fa fa-phone"></i><p>(99) 9999-9999</p>
						</div>
						<div class="contact-mail">
							<i class="fa fa-envelope"></i><p>email@email.com.br</p>
						</div>				
					</div>
					<div class="footer-contact--container open-sans-light left col-lg-4">
						<div class="contact-city">
							<p>Ribeirão Preto -SP</p>
						</div>	
						<div class="contact-phone">
							<i class="fa fa-phone"></i><p>(99) 9999-9999</p>
						</div>
						<div class="contact-mail">
							<i class="fa fa-envelope"></i><p>email@email.com.br</p>
						</div>				
					</div>
				</div>
				<div class="row footer-social col-lg-12">
					<div class="footer-social--title open-sans-semi">
						<h1>siga-nos</h1>
					</div>
					<div class="footer-social--icons">
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-instagram"></i></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-pinterest-p"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copiryght text-center open-sans-regular">
		<p>©2017 All Rights Reserved wDezoito</p>
	</div>
</footer>

 <script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>   
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSUnuBVLFtznJMoLQiCoVO0MMkHmzUN2Y" async defer></script>
 <script src="<?php bloginfo('template_directory'); ?>/js/champs.min.js"></script>  
</body>
</html>
