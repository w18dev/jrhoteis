<?php include('header.php'); ?>

  <div class="row col-lg-12 col-xs-12 blog">
      <?php include ('blog-banner.php'); ?>
        <div class="row col-lg-12 col-xs-12 blog-container">
            <div class="container center">
                <div class="row col-lg-9 left block-left">
                  <?php 
                       $category = (get_query_var('cat'));
                       $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
                       $query = array("post_per_page" => 3,"post_status" => "publish", "paged" => $paged, "cat" => $category);
                         query_posts($query);
                         while(have_posts()): the_post(); 
                            $post = get_post();
                            $conteudo_resumo = getResumeContent($post->post_content);  
                            $post_comment_count   = wp_count_comments($post->ID)->all;  
                            $post_link = get_permalink($post->ID);
                            $post_likes_count   = getFacebookLikesCounter($post_link); 
                            $current = get_cat_name($post->ID);
                  ?>
                    <div class="post col-lg-12">
                        <div class="carousel-block">
                            <div class="carousel">
                                <div class="carousel-posts" id="carousel-blog">
                                    <?php
                                        while ( have_rows('carousel_posts_blog') ) : the_row();
                                          $postId = $post->ID;
                                          $imagem_carousel = get_sub_field('imagem_carousel_posts', $postId); 
                                      ?>
                                      <div class="item">                
                                          <div class="carousel-img">
                                              <img src="<?php echo $imagem_carousel; ?>">
                                          </div>
                                      </div>
                                      <?php
                                         endwhile; 
                                         $paginationArgs = array('mid_size' => 4, 'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'textdomain' ), 'next_text' => __( '<i class="fa fa-angle-right"></i>', 'textdomain' ));
                                        $pagination = get_the_posts_pagination($paginationArgs);
                                      ?>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-11 center">
                            <div class="post-content text-center mongolian">
                                <div class="row post-title">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                                <div class="row post-content--block open-sans-regular col-lg-12">
                                    <div class="post-author col-lg-6 text-right center">
                                        <p>por <?php the_author(); ?> /</p>
                                    </div>
                                    <div class="post-category col-lg-6 text-left center">
                                        <?php the_category(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="post-text open-sans-regular">
                                <p><?php echo $conteudo_resumo; ?></p>
                            </div>
                            <div class="post-link text-center">
                                <a href="<?php the_permalink(); ?>">Leia mais</a>
                            </div>
                            <div class="row col-lg-12 social-share">
                                <div class="col-lg-2 right">
                                    <div class="like left col-lg-6">
                                        <li class="list--item"><?php if($post_likes_count > 0){ ?> <span class="post-item-actions--like has-likes"><i class="fa fa-heart"></i> <?=$post_likes_count?><span class="tooltip"><?=$post_likes_count?> Likes no Facebook</span></span><?php }else{ ?><span class="post-item-actions--like"><i class="fa fa-heart"></i> <?=$post_likes_count?><span class="tooltip"><?=$post_likes_count?> Likes no Facebook</span></span><?php } ?></li>
                                    </div>
                                    <div class="comentario right col-lg-6">
                                        <li class="list--item"><?php if($post_comment_count > 0){ ?> <span class="post-item-actions--comment has-comment"><i class="fa fa-comment"></i> <?=$post_comment_count?><span class="tooltip"><?=$post_comment_count?> Comentários</span></span><?php }else{ ?><span class="post-item-actions--comment"><i class="fa fa-comment"></i> <?=$post_comment_count?><span class="tooltip"><?=$post_comment_count?> Comentário</span></span><?php } ?></li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_query(); ?>
                    <div class="row post-pagination col-lg-12">
                        <div class="text-center"> 
                            <?php echo $pagination; ?>
                        </div>
                    </div>
              </div>
              <?php include('blog-right.php'); ?>
          </div>
      </div>
  </div>



<?php include('footer.php'); ?>