<div class="row col-lg-12 col-xs-12 estadias">
	<div class="container center">
		<div class="row">
			<div class="col-lg-6">
				<div class="estadias-title text-left">
					<h1>Quartos e Suítes</h1>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="estadias-all text-right">
					<a href="">ver todos</a>
				</div>
			</div>
			
		</div>
		<div class="row estadias-posts">
			<div class="row posts col-lg-12">
				<div class="posts-container">
					<?php 
						   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
						   $query = array("post_type" => "categoria", "post_per_page" => 3,"post_status" => "publish", "paged" => $paged);
                           query_posts($query);
                           while(have_posts()): the_post(); 
                           	  $post_ID = $post->ID;
                          	  $post = get_post();
                          	  $imagem = get_field('imagem_quarto', $post_ID);
                          	  $local = get_field('local_quarto', $post_ID);
                              $conteudo_resumo = getResume($post->post_content);  
                              $post_comment_count		= wp_count_comments($post->ID)->all;   
                     ?>
						 <div class="quarto-post col-lg-3 left">
						 	<div class="quarto-post--img">
						 		<img src="<?php echo $imagem; ?>">
						 	</div>
						 	<div class="container open-sans-regular">
							 	<div class="quarto-post--title">
							 		<h1><?php the_title(); ?></h1>
							 	</div>
							 	<div class="row quarto-post--info1">
								 	<div class="quarto-post--hotel left">
								 		<h2><?php echo $local; ?></h2>
								 	</div>
							 	</div>
							 	<div class="row quarto-post--info2">
								 	<div class="quarto-post--services left">
								 		<?php
										    while ( have_rows('servicos') ) : the_row();
											$postId = $post->ID;
											$imagem_icone = get_sub_field('icone_servico', $postId); 
									    ?>
									 		<div class="icon-service left">
									 			<img src="<?php echo $imagem_icone; ?>">
									 		</div>
								 		<?php endwhile; ?>					 		
								 	</div>		
							 	</div>
							 	<div class="row quarto-post--avaliacao">
							 		<div class="avaliacao left">
							 			<p>Avaliações</p>
							 		</div>
							 		<div class="stars right">
							 			<img src="<?php bloginfo('template_directory'); ?>/images/stars-quarto.png">
							 		</div>
							 	</div>
							 	<div class="row quarto-post--link text-center">
							 		<a href="<?php the_permalink(); ?>">Reserve aqui</a>
							 	</div>
						 	</div>
						 </div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>