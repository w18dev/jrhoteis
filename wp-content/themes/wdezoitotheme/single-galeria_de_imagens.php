<?php include('header.php'); ?>

	<?php 
		$post = get_post('213');
		$postID = $post->ID;
		$background = get_field('imagem_de_background', $postID);
	?>
	<div class="row col-lg-12 col-xs-12 col-md-12 page-galeria">
		<div class="cover cover-img" style="background-image: url(<?php echo $background; ?>);">
			<div class="container center">
				<div class="cover-msg text-center open-sans-regular">
					<h1>Galeria</h1>
				</div>
			</div>
		</div>
		<div class="galeria-block">
			<div class="container center">
				<div class="row galeria-links text-center open-sans-regular">
					<nav>
						<?php 
	                        $args = array("nome" => "Galeria", "tipo" => "list-block");
	                        getMenu($args);
	                    ?>
					</nav>
				</div>
				<div class="row galeria-imgs">
					<div class="item col-lg-12">
						<?php 
	                        the_post();
						    while ( have_rows('carousel_de_fotos') ) : the_row();
						    	$post 	= get_post();  
								$postId = $post->ID;
								$imagem_carousel = get_sub_field('imagem_fotos', $postId); 
						?>
							<div class="text-center left">
								<div class="img-post">
									<a data-fancybox="grouped_elements" rel="group1"  href="<?php echo $imagem_carousel; ?>">
										<img src="<?php echo $imagem_carousel; ?>">
										<div class="zoom-block">
											<i class="fa fa-search" aria-hidden="true"></i>
										</div>
									</a>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>





<?php include('footer.php'); ?>
<?php include('modalGaleria.php'); ?>
