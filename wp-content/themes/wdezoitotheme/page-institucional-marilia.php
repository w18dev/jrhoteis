<?php include('header.php'); ?>
	<div class="row institucional">
		<?php
	        $post = get_post('337'); 
	        $postId = $post->ID;
	        $titulo_banner = get_field('titulo_institucional', $postId);
	        $sub_banner = get_field('subtitulo_institucional', $postId);
	        $bkg_banner = get_field('banner_institucional', $postId);
	        $titulo_corpo = get_field('titulo_corpo', $postId);
	        $conteudo_corpo = get_field('conteudo_corpo', $postId);
	        $folder_institucional = get_field('folder_institucional', $postId);

	    ?>
		<div class="row col-lg-12 col-xs-12 inst-background cover2" style="background-image: url(<?php echo $bkg_banner; ?>);">
			<div class="container center">
				<div class="col-lg-8 left inst-block">
					<div class="inst-block--title">
						<h1><?php echo $titulo_banner; ?></h1>
					</div>
					<div class="inst-block--subtitle">
						<h2><?php echo $sub_banner; ?></h2>
					</div>
				</div>
			</div>
		</div>
		<div class="row col-lg-12 col-xs-12 nosso-hotel">
			<div class="container center">
				<div class="col-lg-7">
					<div class="nosso-hotel--block">
						<div class="hotel-title">
							<h1><?php echo $titulo_corpo; ?></h1>
						</div>
						<div class="hotel-content">
							<p><?php echo $conteudo_corpo; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="folder-image">
						<div class="folder-image--block">
							<img src="<?php echo $folder_institucional; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row col-lg-12 col-xs-12 carousel-inst">
			<div class="container center">
				<div class="carousel-inst--block">
					<?php while ( have_rows('carousel_institucional') ) : the_row();
						$imagem_carousel = get_sub_field('imagem_do_carousel', $postId); 
				    ?>
						<div class="item left">
							<div class="item-img">
								<img src="<?php echo $imagem_carousel; ?>">
							</div>
						</div>
					<?php endwhile; ?>
				</div>				
			</div>
		</div>
		<?php include('estadias.php'); ?>
		<?php include('comentarios.php'); ?>
	</div>













<?php include('podemos-ajudar.php'); ?>

<?php include('footer.php'); ?>