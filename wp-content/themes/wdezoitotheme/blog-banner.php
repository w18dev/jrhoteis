<?php 
	$post = get_post('70'); 
    $postId = $post->ID;
?>
<div class="row col-lg-12 col-xs-12 blog-block">
	<div class="banner-carousel">
		<?php
			while ( have_rows('banner_carousel_blog') ) : the_row();
			$banner = get_sub_field('imagem_banner', $postId); 
			$banner_title = get_sub_field('titulo_banner', $postId); 
			$banner_content = get_sub_field('conteudo_banner', $postId); 
	    ?>
			<div class="item cover cover-img" style="background-image: url(<?php echo $banner; ?>)">
				<div class="cover-container container center text-center">
					<div class="cover-title open-sans-regular">
						<h1><?php echo $banner_title; ?></h1>
					</div>
					<div class="cover-content open-sans-light">
						<p><?php echo $banner_content; ?></p>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>