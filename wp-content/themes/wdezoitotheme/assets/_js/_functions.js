/*
##################################
	ANCHOR
##################################
*/
	function anchorScroll(data){
		var device 	= getDevice(),
			nav 	= 100;
		switch(data){
			case "#":
				$("html, body").animate({ scrollTop: 0 }, 1000); 
				break;
			default:
				$("html, body").animate({ scrollTop: $(data).offset().top - nav }, 1000);
				break;
		}
	}
/*
##################################
	//ANCHOR
##################################
*/
/*
##################################
	NAVSCROLLING
##################################
*/
	function navScrolling(){
		var win_height = $(window).height()/2;
		$(window).on('scroll', function(){
			if($(window).scrollTop() >= win_height){
				$('.nav.nav-fixed').addClass("nav-fixed-scrolling");					
			}else{
				$('.nav.nav-fixed').removeClass("nav-fixed-scrolling");					
			}
		});
	}
/*
##################################
	//NAVSCROLLING
##################################
*/
/*
##################################
	GET DEVICE
##################################
*/
	function getDevice(){
		var device_width = $(window).width(),
			result = "desktop";
		if(device_width >= 1200 && device_width <= 1440){
			result = "notebook";
		}else{
			if(device_width > 992 && device_width < 1200){
				result = "netbook";
			}else{
				if(device_width >= 641 && device_width < 992){
					result = "tablet";
				}else{
					if(device_width >= 320 && device_width < 640){
						result = "mobile";
					}
				}				
			}
		}
		return result;
	}
/*
##################################
	//GET DEVICE
##################################
*/


/*
##################################
	CLEAR SELECTS
##################################
*/

function selectClear(id) {
	 var selector = document.getElementById(id),
	 	 itemsSize = selector.length; 
	for (var i = 0; i < itemsSize; i++) { 
		selector.remove(i);
	} 	
}	

/*

###################################
	//CLEAR SELECTS
###################################

*/

	/*
	==================================
		CHECK REQUERID
	==================================
	*/
		function formCheckRequerid(){
			var requerids = document.getElementsByClassName("form-requerid"),
				status = true;
			formFieldClear();
			if(requerids.length > 0){
				for(var i = 0; i < requerids.length; i++){
					if(requerids[i].value == ""){
						status = false;
						formFieldError(requerids[i]);
					}
				}
			}else{
				status = false;
			}
			return status;
		}
	/*
	==================================
		//CHECK REQUERID
	==================================
	*/
	/*
	==================================
		CHECK EMAIL
	==================================
	*/
		function formCheckEmail(data){
			var field = document.getElementById(data),
				value = field.value,
				status = true;
			if(value.indexOf("@") > 0){
				var arroba = value.indexOf("@");
				if(value.indexOf(".") > arroba + 1){	
				}else{
					status = false;
					formFieldError(field);
				}
			}else{
				status = false;
				formFieldError(field);
			}
			return status;
		}
	/*
	==================================
		//CHECK EMAIL
	==================================
	*/
	/*
	==================================
		FIELD
	==================================
	*/
		/*
		---------------------------------
			ERROR
		---------------------------------
		*/
			function formFieldError(field){
				var parent = field.parentNode;
				parent.classList.add("has-error");
			}
		/*
		---------------------------------
			//ERROR
		---------------------------------
		*/
		/*
		---------------------------------
			ERROR
		---------------------------------
		*/
			function formFieldClear(){
				var field = document.getElementsByClassName('form-field');
				for(var i = 0; i < field.length; i++){
					field[i].classList.remove("has-error");
					field[i].classList.remove("has-success");
					field[i].classList.remove("has-warning");
				}
			}
		/*
		---------------------------------
			//ERROR
		---------------------------------
		*/
	/*
	==================================
		//FIELD
	==================================
	*/
/*
##################################
	//FORM
##################################
*/

 $( function() {
    $( "#datepicker" ).datepicker({ onSelect: function(value) { $('.data').val(value) } });
  } );

   $( function() {
    $( "#datepicker2" ).datepicker({ onSelect: function(value) { $('.data').val(value) } });
  } );

/*
====================================
	NEWS MAPS
====================================
*/
	function newMaps(pLat, pLong){
		var latlong = { lat: parseFloat(pLat), lng: parseFloat(pLong) };
		var map = new google.maps.Map(document.getElementById('maps'), {
          center: latlong,
          scrollwheel: false,
          zoom: 15
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlong
        });
	}
/*
====================================
	//NEWS MAPS
====================================
*/

/*
====================================
	FORM CONTATO CONTROL
====================================
*/
	function FormContatoControl(){
		function ResetActiveHoteis(){
			var hoteis = document.getElementsByClassName("hoteis-hotel");
			for(var i = 0; i < hoteis.length; i++){
				var hotel = hoteis[i];
				hotel.setAttribute("class", "hoteis-hotel");
			}
		}
		function AddActiveHotel(id){
			var hotel = document.getElementById(id);
			hotel.setAttribute("class", "hoteis-hotel active");
		}
		function ShowForm(){
			document.getElementById("contato-feedback--success").style.display = "none";
			document.getElementById("contato-feedback--danger").style.display = "none";
			document.getElementById("contato-inputs").style.display = "block";
		}
		var contato_email_to = document.getElementById("contato-form--emailSend");
		$(".hoteis-hotel").on("click", function(e){
			e.preventDefault();
			var hotel_cod 	= $(this).attr('data-cod'),
				hotel_lat 	= document.getElementById("hotel-latitude--"+hotel_cod).value,
				hotel_lng 	= document.getElementById("hotel-longitude--"+hotel_cod).value,
				hotel_email = document.getElementById("hotel-email--"+hotel_cod).value;
			ResetActiveHoteis();
			AddActiveHotel("hotel--"+hotel_cod);
			contato_email_to.value = hotel_email;
			newMaps(hotel_lat, hotel_lng);
		});
		$(".contato-show-form").on("click", function(){
			ShowForm();
		});
	}
/*
====================================
	//FORM CONTATO CONTROL
====================================
*/