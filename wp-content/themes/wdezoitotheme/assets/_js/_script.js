$(window).load(function(){
	ScrollControl();
	GaleriaSingleUx();
	DatapickerBrFormat();
	blockHospedes();

	if(document.getElementById("reserva-select--hospede") != null){
		var hospede_btn_add  	= $('.hospede-combo-button.add'),
			hospede_btn_remove 	= $('.hospede-combo-button.remove');
		hospede_btn_add.on("click", function(){
			var target 			= $(this).attr('data-target'),
				target_value 	= document.getElementById("reserva-hospede-"+target+"--request").value;
			target_value = parseInt(target_value) + 1;
			document.getElementById("reserva-hospede-"+target+"--request").value = target_value;
			document.getElementById("reserva-hospede-"+target+"--count").innerHTML = target_value;
		});
		hospede_btn_remove.on("click", function(){
			var target 			= $(this).attr('data-target'),
				target_value 	= document.getElementById("reserva-hospede-"+target+"--request").value;
			if(parseInt(target_value) > 0){
				target_value = parseInt(target_value) - 1;
				document.getElementById("reserva-hospede-"+target+"--request").value = target_value;
				document.getElementById("reserva-hospede-"+target+"--count").innerHTML = target_value;				
			}
		});
	}

	if(document.getElementsByClassName("carousel-posts") != null){
		var carousel = $(".carousel-posts");
		carousel.owlCarousel({
			items:1,
			nav: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsiveClass:true,
			autoplay: true, 
			autoplayTimeout: 4000,
			loop: true
		});
	}
	if(document.getElementById("carousel-single") != null){
		var carousel_single = $("#carousel-single");
		carousel_single.owlCarousel({
			items:1,
			nav: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsiveClass:true,
			autoplay: true, 
			autoplayTimeout: 4000,
			loop: true
		});
	}
	if(document.getElementsByClassName("carousel-inst--block") != null){
		var carousel_inst = $(".carousel-inst--block");
		carousel_inst.owlCarousel({
			items:4,
			nav: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsiveClass:true,
			autoplayTimeout: 4000,
			loop: true
		});
	}

	if(document.getElementsByClassName("banner-carousel") != null){
	    var carousel_blog = $(".banner-carousel");
		carousel_blog.owlCarousel({
			items:1,
			autoplay: true,
			nav: false,
			mouseDrag: false,
			responsiveClass:true,
			autoplayTimeout: 4000,
			autoplay: true, 
			autoplayTimeout: 4000,
			animateIn: "fadeIn",
			animateOut: "fadeOut",
			autoplayHoverPause: false
		});	
	}
	if(document.getElementsByClassName("carousel-quarto") != null){
	    var carousel_quarto = $(".carousel-quarto");
		carousel_quarto.owlCarousel({
			items:5,
			nav: false,
			loop: true,
			responsiveClass:true,
			animateIn: "fadeIn",
			animateOut: "fadeOut",
			autoplayHoverPause: false
		});	
	}
	$('#nav-left').click(function(){
		carousel_quarto.trigger('prev.owl.carousel');
	});
	$('#nav-right').click(function(){
		carousel_quarto.trigger('next.owl.carousel');
	});
	 $("#Enviar").on("click", function(e){
	 	e.preventDefault();
	 	if(formCheckRequerid()){
	 		$("#Contato").submit();
	 	}
	 });

	$('#open-menu').click(function(){
		$('#close-menu').css({'right' : '0'});
		
	});
	$("#btnClose").on("click", function(){
		$('#close-menu').css({'right' : '-100%'});

	});

	 $('#grid').masonry({
	  });

	 
	if(document.getElementsByClassName("icon-plus") != null){
		if($(".plusCri").click()){
			plusClick(this);
		}
		else{
			if($(".minusCri").click()){
				minusClick(this);	
			}
		}
	}

	if(document.getElementById("page-contato") != null){
		FormContatoControl();
	}

	/* Apply fancybox to multiple items */
	
	$("[data-fancybox]").fancybox({
		baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
			'<div class="fancybox-bg"></div>' +
			'<div class="fancybox-controls">' +
				'<div class="fancybox-infobar">' +
					'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
					'<div class="fancybox-infobar__body">' +
						'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
					'</div>' +
					'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
				'</div>' +
				'<div class="fancybox-buttons">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
				'</div>' +
			'</div>' +
			'<div class="fancybox-slider-wrap">' +
				'<div class="fancybox-slider"></div>' +
			'</div>' +
			'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
		'</div>',
		thumbs     : true,
	});
});

function ScrollControl(){
	var screenTop = $(window).scrollTop();
	if(screenTop > 60){
		$('#navigation').addClass('header-scrolling');
	}else{
		$('#navigation').removeClass('header-scrolling');		
	}
	$(window).on('scroll', function(){
		screenTop = $(window).scrollTop();
		if(screenTop > 60){
			$('#navigation').addClass('header-scrolling');
		}else{
			$('#navigation').removeClass('header-scrolling');
		}
	});
}


function GaleriaSingleUx(){
	$('.item').on('click', function(){
		var idPost = $(this).attr('data-post');
		var img = $("#cat-img--" + idPost).attr('src');

		$("#carousel-bkg").attr('src', img);

		console.log(idPost);
		console.log(img);
	});
}

function DatapickerBrFormat(){
	$.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

	
}

function blockHospedes(){
	if(document.getElementsByClassName("form-field--container-counter") != null){
	
	}
}

function plusClick(e) {
	$(".plusCri").click(function(){
	    var value = +$("#count").text();
	    value++;
	    $("#count").text('' + value);
	});
};

function minusClick(e) {
	$(".minusCri").click(function(){
	    var value = +$("#count").text();
	    value--;
	    $("#count").text('' + value);
	});
};
