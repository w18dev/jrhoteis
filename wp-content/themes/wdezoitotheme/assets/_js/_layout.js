/*
############################################
	LAYOUT	
############################################	
*/
	var Layout = function(){
		this.wHeight = window.innerHeight;
		this.wWidth = window.innerWidth;
	}
	/*
	=================================
		HEIGHT
	=================================
	*/
		/*
		---------------------------------
			FULL
		---------------------------------
		*/
			Layout.prototype.heightFull = function(selector){
				var window_height 	= this.wHeight;
				selector.style.height = window_height+"px";
			}
		/*
		---------------------------------
			//FULL
		---------------------------------
		*/
	/*
	=================================
		//HEIGHT 
	=================================
	*/
	/*
	=================================
		CONTAINER
	=================================
	*/
		/*
		---------------------------------
			CENTER
		---------------------------------
		*/
			Layout.prototype.containerCenter = function(selector, orientation){
				var selector_childs = selector.childNodes,
					window_height 	= this.wHeight,
					window_width 	= this.wWidth,
					childs_count 	= selector_childs.length;
				for(var i = 0; i < childs_count; i++){
					var child_class  	= selector_childs[i].classList;
					if(child_class != undefined){
						var class_count = child_class.length;
						for(var count = 0; count < class_count; count++){
							switch(child_class[count]){
								case "layout-container":
									selector_childs[i].style.float = "left";
									var container_width 	= selector_childs[i].clientWidth,
										container_height 	= selector_childs[i].clientHeight,
										mTop 				= (window_height - container_height)/2,
										mLeft 				= (window_width - container_width)/2;
									switch(orientation){
										case "y":
											selector_childs[i].style.marginTop = mTop+"px";
											//selector_childs[i].style.width = 100+"%";
											break;
										case "x":
											selector_childs[i].style.marginLeft = mLeft+"px";
											break;
										default:
											selector_childs[i].style.marginLeft = mLeft+"px";
											selector_childs[i].style.marginTop = mTop+"px";
											break;
									}
									break;
							}
						}
					}
				}
			}
		/*
		---------------------------------
			//CENTER
		---------------------------------
		*/
	/*
	=================================
		//CONTAINER
	=================================
	*/
	/*
	---------------------------------
		PAGE LOAD
	---------------------------------
	*/
		$(document).ready(function(){
			var layout 			= new Layout(),
				control_layout 	= document.getElementsByClassName("layout-control--js"),
				control_count 	= control_layout.length;
			for(var i = 0; i < control_count; i++){
				var classes 		= control_layout[i].classList,
					classes_count 	= classes.length;
				for(var count = 0; count < classes_count; count++){
					switch(classes[count]){
						case "layout-height--full":
							layout.heightFull(control_layout[i]);
							break;
						case "layout-container--centerY":
							layout.containerCenter(control_layout[i], "y");
							break;
						case "layout-container--centerX":
							layout.containerCenter(control_layout[i], "x");
							break;
						case "layout-container--center":
							layout.containerCenter(control_layout[i]);
							break;
					}
				}
			}			
		});
		$(window).on("resize", function(){
			var layout 			= new Layout(),
				control_layout 	= document.getElementsByClassName("layout-control--js"),
				control_count 	= control_layout.length;
			for(var i = 0; i < control_count; i++){
				var classes 		= control_layout[i].classList,
					classes_count 	= classes.length;
				for(var count = 0; count < classes_count; count++){
					switch(classes[count]){
						case "layout-height--full":
							layout.heightFull(control_layout[i]);
							break;
						case "layout-container--centerY":
							layout.containerCenter(control_layout[i], "y");
							break;
						case "layout-container--centerX":
							layout.containerCenter(control_layout[i], "x");
							break;
						case "layout-container--center":
							layout.containerCenter(control_layout[i]);
							break;
					}
				}
			}
		});
	/*
	---------------------------------
		//PAGE LOAD
	---------------------------------
	*/
	/*
	---------------------------------
		FUNCTION
	---------------------------------
	*/
		function jsLayout(){
			var layout 			= new Layout(),
				control_layout 	= document.getElementsByClassName("layout-control--js"),
				control_count 	= control_layout.length;
			for(var i = 0; i < control_count; i++){
				var classes 		= control_layout[i].classList,
					classes_count 	= classes.length;
				for(var count = 0; count < classes_count; count++){
					switch(classes[count]){
						case "layout-height--full":
							layout.heightFull(control_layout[i]);
							break;
						case "layout-container--centerY":
							layout.containerCenter(control_layout[i], "y");
							break;
						case "layout-container--centerX":
							layout.containerCenter(control_layout[i], "x");
							break;
						case "layout-container--center":
							layout.containerCenter(control_layout[i]);
							break;
					}
				}
			}
		}
	/*
	---------------------------------
		//FUNCTION
	---------------------------------
	*/
/*
############################################
	//LAYOUT	
############################################	
*/
