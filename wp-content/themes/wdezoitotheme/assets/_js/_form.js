$(document).ready(function(){
	/*
	//////////////////////////////////
		Mask Plugin
	//////////////////////////////////
	*/
		$('.form-mask-js').each(function(){
			var m_type = $(this).attr('data-mask');
			switch(m_type){
				case 'celular': $(this).mask("(00) 00000-0000");
								/*$(this).attr('placeholder', "(__) _____-____");*/
								break;
				case 'telefone': $(this).mask("(00) 0000-0000");
								 /*$(this).attr('placeholder', "(__) ____-____");*/
								 break;
				case 'data': $(this).mask("00/00/0000");
								 /*$(this).attr('placeholder', "__/__/____");*/
								 break;
				case 'cpf': $(this).mask("000.000.000-00");
								 /*$(this).attr('placeholder', "___.___.___-__");*/
								 break;
				case 'cnpj': $(this).mask("00.000.000/0000-00");
								 /*$(this).attr('placeholder', "__.___.___/____-__");*/
								 break;
			}
		});

	/*
	//////////////////////////////////
		//Mask Plugin
	//////////////////////////////////
	*/	
	/*
	////////////////////////////////////////////////////
		SELECT
	////////////////////////////////////////////////////
	*/	
		/*
		===================================================
			OPEN > OPTIONS LIST
		===================================================
		*/
			$(".form-select .form-action").on("click", function(){
				if($(this).parent().parent().hasClass("form-select")){
					if(!$(this).parent().parent().hasClass("active")){
						$(this).parent().parent().addClass("active");
					}else{
						$(this).parent().parent().removeClass("active");					
					}
				}
				var div  = $(this).parent().parent();
				$("body").on("click", function (e) {
			        if (div.has(e.target).length || e.target == div[0])
			            return;
			        div.removeClass('active');
			    });
			});
		/*
		===================================================
			//OPEN > OPTIONS LIST
		===================================================
		*/   

		/*
		===================================================
			CLOSE > OPTIONS LIST
		===================================================
		*/
			$(".form-select .field-list > li a").on("click", function(e){
				e.preventDefault();
				var itemValue = $(this).attr("data-value");
				document.getElementById("reserva-hotel--request").value = itemValue;
				$(this).parent().parent().parent().parent().removeClass("active");
			});
		/*
		===================================================
			//CLOSE > OPTIONS LIST
		===================================================
		*/
	/*
	////////////////////////////////////////////////////
		//SELECT
	////////////////////////////////////////////////////
	*/
});
