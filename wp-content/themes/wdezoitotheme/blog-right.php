<div class="col-lg-3 right block-right">
	<div class="container center">
		<div class="search-blog">
			<form>
				<div class="form-field blog">
					<input type="text" name="searchBlog" placeholder="Pesquisar">
					<div class="search-blog--icon">
						 <i class="fa fa-search"></i>
					</div>
				</div>
			</form>
		</div>
		<div class="destaque-blog">
			<div class="destaque-blog--title text-center open-sans-regular">
				<h1>em destaque</h1>
			</div>
			<div class="destaque-post">
				<?php
					$query_dest = new WP_Query(array(
		                    'v_orderby'   => 'desc', // Ordena do mais visitado para o menos visitado.
		                    'numberposts' => 1
		                  )
	                  );
					if($query_dest->have_posts()): 
						$query_dest->the_post();
					    $postDest = get_post()->ID;
					    $dest_resumo = getResume($post->post_content);
					    $dest_title = getResume($post->post_title);
					    while ( have_rows('carousel_posts_blog') ) : the_row();
	              	  		$dest_img = get_sub_field('imagem_carousel_posts', $postDest); 
	              	  	endwhile; 
				?>
					<div class="destaque-post--img">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $dest_img; ?>"></a>
					</div>
					<div class="destaque-post-container">
						<div class="destaque-post--title text-center mongolian">
							<h1><?php echo $dest_title; ?></h1>
						</div>
						<div class="open-sans-regular">
							<div class="destaque-post--content">
								<p><?php echo $dest_resumo; ?></p>
							</div>
							<div class="destaque-post--link">
								<a href="<?php the_permalink(); ?>">LEIA MAIS</a>
							</div>	
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="popular-blog">
			<div class="popular-blog--title open-sans-regular text-center">
				<h1>Mais populares</h1>
			</div>
			<?php
                  $query_popular = new WP_Query(array(
	                    'v_orderby'   => 'desc', // Ordena do mais visitado para o menos visitado.
	                  )
                  );

                  if($query_popular->have_posts()):
                  		while($query_popular->have_posts()):
              			  $query_popular->the_post();	
              			  $postId = get_post()->ID;
	                  	  $conteudo_resumo = getResume($post->post_content);
              			  while ( have_rows('carousel_posts_blog') ) : the_row();
	                  	  		$popular_image = get_sub_field('imagem_carousel_posts', $postId); 
					      endwhile;
              ?>
				<div class="row popular-blog--posts col-lg-12">
					<a href="<?php the_permalink(); ?>">
						<div class="col-lg-3 left">
							<div class="post-img">
								<img src="<?php echo $popular_image; ?>">
							</div>
						</div>
						<div class="col-lg-8 right">
							<div class="post-title mongolian">
								<h1><?php the_title(); ?></h1>
							</div>
							<div class="post-content">
								<p><?php echo $conteudo_resumo; ?></p>
							</div>
						</div>
					</a>
				</div>
			<?php endwhile; endif; ?>
		</div>
		<div class="categorias-blog">
			<div class="categorias-blog--title text-center open-sans-regular">
				<h1>Categorias</h1>
			</div>
			<div class="categorias">
				<nav>
					<ul>
						<?php
                            $categories = get_categories();
                            foreach($categories as $category) {
                            	if(get_category_link( $category->term_id ) == UrlAtual()){
                                	echo '<li><a class="active" href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name . ' ('.$category->count.')'.'</a> ';
                            	}
                            	else{
                            		echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name . ' ('.$category->count.')'.'</a> ';
                            	}
                            }
                        ?>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>