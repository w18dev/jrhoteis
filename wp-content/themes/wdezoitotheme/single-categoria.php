<?php include('header.php'); ?>
	<div class="row col-lg-12 col-xs-12 col-md-12 single-quarto">
		<div class="container center">
			<div class="row block-left col-lg-9 left">
				<?php
					the_post();
					$postID = $post->ID;
				?>
				<div class="carousel-bkg">
					<img src="" id="carousel-bkg">
				</div>
				<div class="carousel-block">
					<div class="nav-left">
						<button id="nav-left"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Seta-esquerda.png"></button>
					</div>
					<div class="row carousel-quarto col-lg-12">
						<?php
							while ( have_rows('carousel_quarto') ) : the_row();
								$postId = $post->ID;
								$imagem_icone = get_sub_field('imagem_carousel', $postId);
						?>
							<div class="item left" data-post="<?php echo $postId; ?>">
								<div class="item-img">
									<img src="<?php echo $imagem_icone; ?>" id="cat-img--<?php echo $postId; ?>">
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="nav-right">
						<button id="nav-right"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Seta-direita.png"></button>
					</div>
				</div>
				<div class="block-left--container">
					<div class="content-desc open-sans-regular">
						<div class="content-desc--block center">
							<div class="content-desc--title text-center">
								<h1><?php the_title(); ?></h1>
							</div>
						</div>
						<div class="content-desc--text">
							<p><?php the_content(); ?></p>
						</div>
						<div class="row content-esp">
							<div class="content-esp--block left">
								<p>
									MÁX. DE PESSOAS 1
								</p>
							</div>
							<div class="content-esp--block left">
								<p>
									CAMAS 1
								</p>
							</div>
							<div class="content-esp--block left">
								<p>
									TAMANHO DAS CAMAS 1 Casal
								</p>
							</div>
							<div class="content-esp--block left">
								<p>
									TAMANHO DO QUARTO 44m²
								</p>
							</div>
						</div>
					</div>
					<div class="content-ameni">
						<div class="row content-ameni--title">
							<h1>AMENIDADES</h1>
						</div>
						<div class="row ameni-block">
							<div class="ameni-block--text left">
								<p>Toalhas</p>
							</div>
							<div class="ameni-block--text left">
								<p>Telefone</p>
							</div>
							<div class="ameni-block--text left">
								<p>Guarda-roupa/armário</p>
							</div>
							<div class="ameni-block--text left">
								<p>Serviço de despertar</p>
							</div>
							<div class="ameni-block--text left">
								<p>Vista da cidade</p>
							</div>
							<div class="ameni-block--text left">
								<p>Sofá</p>
							</div>
							<div class="ameni-block--text left">
								<p>TV</p>
							</div>
							<div class="ameni-block--text left">
								<p>DVD player</p>
							</div>
							<div class="ameni-block--text left">
								<p>Banheiro</p>
							</div>
							<div class="ameni-block--text left">
								<p>Vaso sanitário</p>
							</div>
							<div class="ameni-block--text left">
								<p>Canais a cabo</p>
							</div>
							<div class="ameni-block--text left">
								<p>Frigobar</p>
							</div><div class="ameni-block--text left">
								<p>Frigobar</p>
							</div>
							<div class="ameni-block--text left">
								<p>Ar-condicionado</p>
							</div>
							<div class="ameni-block--text left">
								<p>Chuveiro</p>
							</div>
							<div class="ameni-block--text left">
								<p>Banheira de hidromassagem</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row single-quarto--comments">
					<?php include('comments.php'); ?>
				</div>
			</div>
			<div class="row block-right col-lg-3 left">
				<div class="block-right--content">
					<div class="info-right">
						<div class="row info-block">
							<div class="hotel-icon left">
								<img src="<?php bloginfo('template_directory')?>/images/icons/hotel.png">
							</div>
							<div class="hotel-name text-left">
								<h1>JR HOTEL<br/> PRESIDENTE PRUDENTE - SP</h1>
							</div>
						</div>
						<div class="row info-block">
							<div class="hotel-icon left">
								<img src="<?php bloginfo('template_directory')?>/images/icons/quarto.png">
							</div>
							<div class="hotel-name text-left">
								<h1>Apartamento Executivo</h1>
							</div>
						</div>
						<div class="row info-block--avaliacao">
							<div class="col-lg-12">
								<div class="col-lg-7">
									<div class="left">
										<p>Avaliações</p>
									</div>
									<div class="left">
										<img src="<?php bloginfo('template_directory')?>/images/icons/stars.png">
									</div>
								</div>
								<div class="right text-right col-lg-5">
									<p>Comentários 8</p>
								</div>
							</div>
						</div>
						<div class="row info-block--services">
							<div class="row services-title">
								<div class="left">
									<img src="<?php bloginfo('template_directory')?>/images/icons/star.png">
								</div>
								<div class="left">
									<p>Serviços</p>
								</div>
							</div>
							<div class="row services-block col-lg-12">
								<div class="col-lg-4 text-center container">
									<div class="service-icon">
										<img src="<?php bloginfo('template_directory')?>/images/icons/coffee2.png" alt="">
									</div>
									<div class="service-text">
										<p>Serviço de Quarto</p>
									</div>
								</div>
								<div class="col-lg-4 text-center container">
									<div class="service-icon">
										<img src="<?php bloginfo('template_directory')?>/images/icons/wifi2.png" alt="">
									</div>
									<div class="service-text">
										<p>Wifi</p>
									</div>
								</div>
								<div class="col-lg-4 text-center container">
									<div class="service-icon">
										<img src="<?php bloginfo('template_directory')?>/images/icons/tv.png" alt="">
									</div>
									<div class="service-text">
										<p>TV a cabo</p>
									</div>
								</div>
							</div>
						</div>
						<div class="info-block--advanced">
							<div class="advanced-block">
								<div class="row advanced-block--title text-center">
									<h1>Filtro Avançado</h1>
								</div>
								<div class="row advanced-block--form">
									<form>
										<div class="form-block">
											<div class="form-block--input">
												<input type="text" name="txtCheckOut" placeholder="Check-In" id="datepicker" class="datepicker">
												<div class="form-block--icon">
													<img src="<?php bloginfo('template_directory')?>/images/icons/calendario.png" alt="">
												</div>
											</div>
										</div>
										<div class="form-block">
											<div class="form-block--input">
												<input type="text" name="txtCheckOut" placeholder="Check-out" id="datepicker2" class="datepicker">
												<div class="form-block--icon">
													<img src="<?php bloginfo('template_directory')?>/images/icons/calendario.png" alt="">
												</div>
											</div>
										</div>
										<div class="form-block">
											<div class="form-block--input">
												<input type="text" name="" value="" placeholder="Hóspedes" onfocus="myFunction(this)">
												<div class="form-block--icon">
													<img src="<?php bloginfo('template_directory')?>/images/icons/user.png" alt="">
												</div>
												<div class="row form-field--container-counter col-lg-12">
						                    		<div class="container">
						                    			<div class="row counter-adultos col-lg-12">
						                    				<div class="left col-lg-8">
						                    					<input type="hidden" name="" value="0"><span id="count">0</span><span>adutos</span>
						                    				</div> 
						                    				<div class="right col-lg-3 icons">
						                    					<div class="text-left col-lg-6 left icon-plus">
						                    						<button type="button"><i class="fa fa-plus"></i></button>
						                    					</div>
						                    					<div class="text-right col-lg-6 left icon-minus">
						                    						<button type="button"><i class="fa fa-minus"></i></button>
						                    					</div>
						                    				</div>
						                    			</div>
						                    			<div class="row counter"> 
						                    				<div class="col-lg-5">
							                    				<input type="hidden" name="" value="0"><span id="count">0</span><span>crianças</span>
						                    				</div> 
						                    				<div class="right col-lg-7 icons">
						                    					<div class="col-lg-6">
						                    						<span>2 a 12 anos</span>
						                    					</div>
						                    					<div class="text-left col-lg-3 left icon-plus">
						                    						<button type="button"><i class="fa fa-plus"></i></button>
						                    					</div>
						                    					<div class="text-right col-lg-3 left icon-minus">
						                    						<button type="button"><i class="fa fa-minus"></i></button>
						                    					</div>
						                    				</div>
						                    			</div> 
						                    			<div class="row counter">  
						                    				<div class="col-lg-5">
							                    				<input type="hidden" name="" value="0"><span id="count">0</span><span>bebês</span>
						                    				</div> 
						                    				<div class="right col-lg-7 icons">
						                    					<div class="col-lg-6">
						                    						<span>menor de 2</span>
						                    					</div>
						                    					<div class="text-left col-lg-3 left icon-plus">
						                    						<button type="button"><i class="fa fa-plus"></i></button>
						                    					</div>
						                    					<div class="text-right col-lg-3 left icon-minus">
						                    						<button type="button"><i class="fa fa-minus"></i></button>
						                    					</div>
						                					</div>
						                    			</div>              			
						                    		</div>
						                    	</div>
											</div>
										</div>
										<div class="form-block">
											<div class="form-block--button text-center">
												<button type="submit" name="btnReservar">Reservar Agora</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="block-right">
					<div class="block-right--content">
						<div class="info-right"> 
							<div class="quartos-similares">
								<div class="row quartos-similares--title text-center">
									<h1>QUARTOS SIMILARES</h1>
								</div>
								<div class="row quartos-similares--posts">
									<div class="row posts-block col-lg-12">
										<div class="posts-block--img left col-lg-4">
											<img src="<?php bloginfo('template_directory')?>/images/quarto-cat.jpg" alt="">
										</div>
										<div class="posts-block--content left col-lg-8 container">
											<div class="content-title">
												<h1>Apartamento Duplo Executivo </h1>
											</div>
											<div class="content-stars col-lg-6 left">
												<img src="<?php bloginfo('template_directory')?>/images/stars-quarto.png" alt="">
											</div>
										</div>
									</div>
									<div class="row posts-block col-lg-12">
										<div class="posts-block--img left col-lg-4">
											<img src="<?php bloginfo('template_directory')?>/images/quarto-cat.jpg" alt="">
										</div>
										<div class="posts-block--content left col-lg-8 container">
											<div class="content-title">
												<h1>Apartamento Duplo Executivo </h1>
											</div>
											<div class="content-stars col-lg-6 left">
												<img src="<?php bloginfo('template_directory')?>/images/stars-quarto.png" alt="">
											</div>
										</div>
									</div>
									<div class="row posts-block col-lg-12">
										<div class="posts-block--img left col-lg-4">
											<img src="<?php bloginfo('template_directory')?>/images/quarto-cat.jpg" alt="">
										</div>
										<div class="posts-block--content left col-lg-8 container">
											<div class="content-title">
												<h1>Apartamento Duplo Executivo </h1>
											</div>
											<div class="content-stars col-lg-6 left">
												<img src="<?php bloginfo('template_directory')?>/images/stars-quarto.png" alt="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>








<?php include('footer.php'); ?>
