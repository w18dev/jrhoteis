<?php
	function getInfoRede(){
		$result = null;
		$args 	= array('post_type' => 'rede', 'name' => 'informacoes');
		$query 	= new WP_Query($args);
		if($query->have_posts()){
			$query->the_post();
			$_post = get_post();
			$result["logo"] 				= get_field('rede-logo', $_post->ID);
			$result["email"] 				= get_field('rede-email', $_post->ID);
			$result["social"]["facebook"] 	= get_field('rede-social--facebook', $_post->ID);
			$result["social"]["twitter"]	= get_field('rede-social--twitter', $_post->ID);
			$result["social"]["instagram"]	= get_field('rede-social--instagram', $_post->ID);
			$result["social"]["pinterest"]	= get_field('rede-social--pinterest', $_post->ID);			
		}
		wp_reset_query();
		return $result;
	}
	/*
	=====================================
		HOTEIS
	=====================================
	*/
		class Hoteis{
			/*
			===============================
				GET > HOTEIS
			===============================
			*/
				static function getHoteis(){
					$result = null;
					$args 	= array('post_type' => 'hoteis');
					$query 	= new WP_Query($args);
					if($query->have_posts()){
						$i = 0;
						while($query->have_posts()){
							$query->the_post();
							$_hotel = get_post();
							$result[$i]["id"] 						= $_hotel->ID;
							$result[$i]["foto"]			 			= get_field("hotel-foto", $_hotel->ID);
							$result[$i]["contato"]["telefone"] 		= get_field("hotel-telefone", $_hotel->ID);
							$result[$i]["contato"]["email"] 		= get_field("hotel-email", $_hotel->ID);
							$result[$i]["localizacao"]["cep"] 		= get_field("hotel-cep", $_hotel->ID);
							$result[$i]["localizacao"]["cidade"]	= $_hotel->post_title;
							$result[$i]["localizacao"]["endereco"] 	= get_field("hotel-endereco", $_hotel->ID);
							$result[$i]["localizacao"]["bairro"]	= get_field("hotel-bairro", $_hotel->ID);
							$result[$i]["localizacao"]["latitude"]	= get_field("hotel-latitude", $_hotel->ID);
							$result[$i]["localizacao"]["longitude"]	= get_field("hotel-longitude", $_hotel->ID);
							$result[$i]["social"]["facebook"]		= get_field("hotel-link--facebook", $_hotel->ID);
							$result[$i]["social"]["instagram"]		= get_field("hotel-link--instagram", $_hotel->ID);
							$result[$i]["social"]["twitter"]		= get_field("hotel-link--twitter", $_hotel->ID);
							$result[$i]["social"]["pinterest"]		= get_field("hotel-link--pinterest", $_hotel->ID);
							$i++;
						}
					}
					wp_reset_query();
					return $result;
				}
			/*
			===============================
				//GET > HOTEIS
			===============================
			*/
			/*
			===============================
				GET > CIDADES
			===============================
			*/
				static function getCidades(){
					$result = null;
					$args 	= array('post_type' => 'hoteis');
					$query 	= new WP_Query($args);
					if($query->have_posts()){
						$i = 0;
						while($query->have_posts()){
							$query->the_post();
							$_hotel = get_post();
							$result[$i]	= $_hotel->post_title;
							$i++;
						}
					}
					wp_reset_query();
					return $result;
				}
			/*
			===============================
				//GET > CIDADES
			===============================
			*/
		}
	/*
	=====================================
		//HOTEIS
	=====================================
	*/
	/*
	=====================================
		SERVICOS
	=====================================
	*/
		class Servicos{
			static function getServicos(){
				$result = null;
				$args 	= array("post_type" => "servico", "order" => "ASC");
				$query 	= new WP_Query($args);
				if($query->have_posts()){
					$i = 0;
					while($query->have_posts()){
						$query->the_post();
						$_servico 					= get_post();
						$result[$i]["id"] 			= $_servico->ID;
						$result[$i]["icone"] 		= get_field('servico-icone', $_servico->ID);
						$result[$i]["nome"] 		= $_servico->post_title;
						$result[$i]["descricao"] 	= get_field('servico-descricao', $_servico->ID);
						$i++;
					}
				}
				return $result;
			}
		}
	/*
	=====================================
		//SERVICOS
	=====================================
	*/
	/*
	=====================================
		COMENTARIOS
	=====================================
	*/
		class Comentarios{
			static function getComentariosHome(){
				$result = null;
				return $result;
			}
		}
	/*
	=====================================
		//COMENTARIOS
	=====================================
	*/
	function getResumeContent($content){
		if(strlen($content) > 150){
			$content  = strip_tags($content);
            $content  = trim($content);
			$content = substr($content, 0, 500);
			$pos = strrpos($content, " ");
			$resume = substr($content, 0, $pos)."...";
		}else{
			$resume = $content;
		}
		return $resume;
	}
	function getResume($content){
		if(strlen($content) > 150){
			$content  = strip_tags($content);
            $content  = trim($content);
			$content = substr($content, 0, 100);
			$pos = strrpos($content, " ");
			$resume = substr($content, 0, $pos)."...";
		}else{
			$resume = $content;
		}
		return $resume;
	}
	function getResumeMin($content){
		if(strlen($content) > 150){
			$content  = strip_tags($content);
            $content  = trim($content);
			$content = substr($content, 0, 60);
			$pos = strrpos($content, " ");
			$resume = substr($content, 0, $pos)."...";
		}else{
			$resume = $content;
		}
		return $resume;
	}
	add_theme_support("menus");

	/*
	================================
		MENU
	================================
	*/
		function getMenu($p_args){
			switch ($p_args["tipo"]) {
				case 'list-inline':
					$menuClass 		= "menu menu-list--inline";
					if($p_args["menu_class"] != null){
						$menuClass = $menuClass." ".$p_args["menu_class"];
					}
					$args = array(
							'menu' 			=> $p_args["nome"],
							'menu_id' 			=> $p_args["nome"],
							'menu_class'	=> $menuClass,
							'container'		=> "ul"
						);
					wp_nav_menu($args);
					break;
				case 'list-block':
					$menuClass 		= "menu menu-list--block";
					if($p_args["menu_class"] != null){
						$menuClass = $menuClass." ".$p_args["menu_class"];
					}
					$args = array(
							'menu' 			=> $p_args["nome"],
							'menu_class'	=> $menuClass,
							'container'		=> "ul"
						);
					wp_nav_menu($args);
					break;
			}
		}
	/*
	================================
		//MENU
	================================
	*/
	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

	function special_nav_class ($classes, $item) {
	   if (in_array('current-menu-item', $classes) ){
	       $classes[] = 'active';
	   }
	   return $classes;
	}
?>