<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'w18_jr_hoteis');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vk98E(wn`6dXu`Znd.X^~GIwsb(20f*9Hy)Xlb<T`y~UV4x.NQ=Fyghtc=FYRjoM');
define('SECURE_AUTH_KEY',  '^:6:;/s%2Hb3*.}T#sq]2vR6b?>z%HV_..TmH7O-AmHe+DZ9n}irSmy`9Cg k90O');
define('LOGGED_IN_KEY',    'X:tq~W9IlKi1^o`%CyEjR_*@#AmC;4#jX+zPv|4&9J3e^6>0j%j@B|QSS{TN{nD ');
define('NONCE_KEY',        'jIbDY6#)ti<#],TZ_gZ3R{Z7/Zj|=9Fi>_5)P2B?rF#}{MB8sOl9]Yieu;ky[7n4');
define('AUTH_SALT',        'J.k}h]-KHyHVCA{GqSS8,r0*N/31i-|/-9DtW.&.[`xy$F:IX)#kvy~}Yp:D$>b^');
define('SECURE_AUTH_SALT', '1Q6:E/o&?S2z/[T.`H6RtbK.fH.nb!nYVRAyOT2:djXeFH~slFJ%`4Rs$a?*+]`D');
define('LOGGED_IN_SALT',   'rF0Jo7RU=90TtZ-90|t@g?fN|l,lMwCY`4C>9=[E-;EdvxeiFv@[sQ;xI?*@@{hd');
define('NONCE_SALT',       'wqZ%Ym,o*+[f8wn>GTe D:P6n9n:%#4rSVq]eG_``<$<j=GoTs-X71<)NS#caEME');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
